<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Donasi</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="asset/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="asset/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="asset/dist/css/reciept.css">
    <link rel="stylesheet" href="asset/dist/css/donation.css">
    <style>
        p.large {
            padding-left: 10px;
        }
</style>
</head>

<body>

    <div class="container">
        <div class="reciept">
            <div class="row" style="margin-top:10px;">
                <div class="col-md-12">

                    <center>
                        <h4 class="heading">Form Konfirmasi</h4>
                    </center>
                </div>

            </div>
            <br/>
             <form method="POST" action="confirmProcess" role="form" enctype="multipart/form-data"> 
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-3 control" for="">Nama Pemilik Rekening Pengirim</label>
                        <div class="col-md-9">
                            <input id="nameDonatur" name="nameDonatur" type="text" placeholder="Rekening donatur" class="form-control" required=true>
                        </div>
                    </div>
                    <br/>

                    <div class="form-group">
                        <label class="col-md-3 control" for="">ID Donasi</label>
                        <div class="col-md-9">
                            <input id="idDonasi" name="idDonasi" type="text" placeholder="ID Donasi" class="form-control" required=true>
                        </div>
                    </div>
                </div>
            </div>

                <br/>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-3 control" for="name">Upload Bukti Transfer</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                 <input type="text" class="form-control" readonly  placeholder="Bukti Transfer" id="filename" name="filename" required=true>
                                    <label class="input-group-btn">
                                        <span class="btn btn-primary">
                                       Upload&hellip; <input type="file" style="display: none;" name="transferFile" id="transferFile" onchange="setfilename2(this.value);" accept="image/x-png,image/jpeg,image/jpg">
                                        </span>
                                    </label>

                                   
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                 <div class="col-md-12">
                 <div class="form-group">
                        <label class="col-md-3 control" for="name"></label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <br>
                                 <p class="large">Jenis File yang diperbolehkan: .jpg, .jpeg, dan .png</p> 
                            </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top:80px;">
                <div class="col-md-9">
                </div>
                <div class="col-md-3">
                    
                </div>
            </div>

           
                <div class="row">                   
                    <div class="col-md-2 col-md-offset-5">
                        <input type="submit" class="btn btn-primary btn-block" value="Konfirmasi">
                     </div>
                     
                </div>
            </form>
            <br>
        </div>
    </div>
    </div>
    </div>

    <script src="asset/dist/js/jquery-1.11.1.min.js"></script>
    <script src="asset/dist/js/bootstrap.min.js"></script>
    <script>
        function setfilename2(val)
        {
            var fileName2 = val.substr(val.lastIndexOf("\\")+1, val.length);
            document.getElementById("filename").value = fileName2;
        }
    </script>
</body>

</html>