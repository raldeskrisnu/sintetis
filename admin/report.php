      <div class="col-md-9">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Laporan</h3>
          
                  <div class="btn-group pull-right" style="padding-bottom: 10px">
                      <button type="button" class="btn btn-info">Export</button>
                      <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                          <span class="caret"></span>
                          <span class="sr-only">Toggle Dropdown</span>
                      </button>
                    <ul class="dropdown-menu" role="menu" id="export-menu">
                          <li id="export-to-excel"><a href="#">Export to .xls</a></li>
                          <li class="divider"></li>
                          <li id="export-to-csv"><a href="#">Export to .csv</a></li>
                    </ul>
                  </div>

          <div class="box-body">
            <form action="createExcel" method="post" id="export-form">
                <input type="hidden" value='' id='hidden-type' name='ExportType'/>
            </form>

            <table id="list" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>Produk</th>
                  <th>Jumlah</th>
                  <th>Metode</th>
                  <th>Status</th>
                  <th>Tanggal</th>
                  <th>Hadiah</th>
                  <th>Link</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $selectdb = mysql_select_db($dbname);
                $sql = "SELECT a.doku_donatur_id,a.fullname,a.product,a.email, a.telp, a.areacode, a.city,a.country, a.price,a.method,a.payment_status,a.paymentdatetime, a.donatype, b.gift_title FROM doku_donatur a LEFT JOIN doku_gift b ON b.doku_gift_id=a.doku_gift_id";
                
                //$result = $conn->query($sql);
                $paymentchannel = array(
                  '02' => 'Mandiri ClickPay',
                  '04' => 'DOKU Wallet',
                  '05' => 'ATM Permata VA Lite',
                  '06' => 'BRI e-Pay',
                  '07' => 'ATM Permata VA',
                  '08' => 'Mandiri Multipayment LITE',
                  '09' => 'Mandiri Multipayment',
                  '14' => 'Alfagroup',
                  '15' => 'Credit Card Visa/Master Multi Currency',
                  '16' => 'Credit Card Tokenization',
                  '17' => 'Recurring Payment',
                  '18' => 'KlikPayBCA',
                  '19' => 'CIMB Clicks',
                  '21' => 'Sinarmas VA Full',
                  '22' => 'Sinarmas VA Lite',
                  '23' => 'MOTO',
                  '' => 'Undefined'
                );

                if($result = @mysql_query($sql)){
                    if(mysql_num_rows($result) > 0){
                        while($row = mysql_fetch_array($result)){

                    ?>
                        <tr>
                          <td><?php echo $row['fullname'] ?></td> 
                          <td><?php echo $row['product'] ?></td> 
                          <td><?php  
                                echo "Rp " . number_format($row['price'],0,'.','.'); 
                            ?></td>
                          <td><?php echo $row['method'] ?></td> 
                          <td><?php echo $row['payment_status'] ?></td> 
                          <td><?php echo date('F j, Y H:i',strtotime($row['paymentdatetime'])) ?></td> 
                          <td><?php echo $row['gift_title'] ?></td> 

                           <td><button type = "button" class="btn btn-primary" data-toggle="modal" data-target="#<?php echo $row['doku_donatur_id'] ?>" id="exampleModal">Check Status</button></td>
                        </tr>

                        <div class="modal fade" id="<?php echo $row['doku_donatur_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
                             <form action="confirmationProcess" method="post" id="clickform" class="form-horizontal" enctype="multipart/form-data">
                                   <div class="modal-dialog">
                                        <div class="modal-content">
                                              <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="modal" ><font face="Comic Sans MS"><?php echo "Detail Donatur"; ?> </font></h4>
                                              </div>
                                              <div class="modal-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="idDonasi">ID Donasi </label>  
                                                        <div class="col-md-9">
                                                              <input type="text" id="idonatur" name="idonatur" value="<?php 
                                                                    $testing = $row['doku_donatur_id'];
                                                                    echo $testing; ?>" class="form-control input-md" readonly>
                                                        </div>
                                                    </div>
                                                    <p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="namatopic">Nama Individu / Lembaga </label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['fullname'] ?>" class="form-control input-md" readonly>
                                                        </div>
                                                    </div>
                                                    <p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="namatopic">Jenis </label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['donatype'] ?>" class="form-control input-md" readonly>
                                                        </div>
                                                    </div>
                                                    <p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="namatopic">Email </label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['email'] ?>" class="form-control input-md" readonly>
                                                        </div>
                                                    </div>
                                                    <p>&nbsp;</p>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="namatopic">Jumlah Donasi </label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="namatopic" name="namatopic" value="<?php echo "Rp " . number_format($row['price'],0,'.','.'); ?>" class="form-control input-md" readonly>
                                                        </div>
                                                    </div>
                                                    <p>&nbsp;</p> 

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="namatopic">Nomor Telp </label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['telp'] ?>" class="form-control input-md" readonly>
                                                        </div>
                                                    </div>
                                                  <p>&nbsp;</p> 

                                                  <div class="form-group">
                                                        <label class="col-md-3 control-label" for="namatopic">Kode Area </label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['areacode'] ?>" class="form-control input-md" readonly>
                                                        </div>
                                                  </div>
                                                  <p>&nbsp;</p> 

                                                  <div class="form-group">
                                                        <label class="col-md-3 control-label" for="namatopic">Kota </label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['city'] ?>" class="form-control input-md" readonly>
                                                        </div>
                                                  </div>
                                                  <p>&nbsp;</p> 

                                                  <div class="form-group">
                                                        <label class="col-md-3 control-label" for="namatopic">Negara </label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['country'] ?>" class="form-control input-md" readonly>
                                                        </div>
                                                  </div>
                                                  <p>&nbsp;</p> 

                                                  <div class="form-group">
                                                        <label class="col-md-3 control-label" for="namatopic">Appreciation Gift </label>  
                                                        <div class="col-md-9">
                                                            <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['gift_title'] ?>" class="form-control input-md" readonly>
                                                        </div>
                                                  </div>
                                                  <p>&nbsp;</p>

                                              </div>

                                              <div class="modal-footer">
                                                  <p> © Copyright 2018. sintesis.ti.or.id</p>
                                              </div>
                                        </div>
                                   </div>
                             </form>
                        </div>
                    <?php
                        }
                    } else {
                         echo "0 results";
                    }

                }
                
                ?>
              </tbody>
              
            </table>
          </div>
        </div>
       
        </div>