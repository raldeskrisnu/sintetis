        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Buat Baru Hadiah</h3>
            </div>
            <form action="gift_process.php" role="form" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputTitle">Nama Hadiah</label>
                  <input type="text" class="form-control" id="inputTitle" placeholder="Masukkan nama hadiah" name="gift_title">
                </div>
                <textarea class="textarea" placeholder="Masukkan deskripsi" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="gift_message"></textarea>
                <div class="form-group">
                  <div class="col-md-3">
                      <label for="gift_file">Gambar</label>
                      <input type="file" id="gift_file" name="gift_file" accept="image/x-png,image/jpeg,image/jpg" />
                      <p class="help-block"><img src="http://placehold.it/200x200" id="blah" name="gift_picture" alt="your image" width="200" height="200" /></p>
                  </div>
                  <div class="col-md-3">
                      <select class="form-control" name="option" id="option">
                        <option value="" selected="true" disabled="disabled">Kategori</option>
                        <option value="Donasi Rutin">Donasi Rutin</option>
                        <option value="Donasi Sekali">Donasi Sekali</option>
                        <option value="Donasi Program">Donasi Program</option>
                      </select>
                  </div>
                  <div class="col-md-4">
                  <div class="checkbox">
                      <label>
                          <input type="checkbox" name="isactive" value="1"> Aktif dan publikasikan
                      </label>
                  </div>
                </div>
              </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary" name="simpan" value="simpan">Simpan</button>
                <button type="submit" class="btn btn-success" name="simpan" value="tambah">Simpan dan Tambah</button>
                <a class="btn btn-danger" href="index.php?page=gift">Batal</a>
              </div>
            </form>
          </div>
        </div>