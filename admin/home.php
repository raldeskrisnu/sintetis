        <?php
        $selectdb = mysql_select_db($dbname);
        $sql1 = "SELECT COUNT(*) AS successResult FROM doku_donatur WHERE payment_status='SUCCESS'";
        if($result1 = @mysql_query($sql1)){
          while($row1 = mysql_fetch_array($result1)){
            $countSuccess = $row1['successResult'];
          }
        }

        $sql2 = "SELECT COUNT(*) AS allRecord FROM doku_donatur WHERE 1=1";
        if($result2 = @mysql_query($sql2)){
          while($row2 = mysql_fetch_array($result2)){
            $allRecord = $row2['allRecord'];
          }
        }

        $percentage = $countSuccess/$allRecord*100;
        
        ?>
        <div class="col-md-3">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?=$countSuccess?></h3>
              <p>Transaksi Sukses</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?=number_format($percentage,2,'.',',')?><sup style="font-size: 20px">%</sup></h3>
              <p>Persentase Kesuksesan</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?=$allRecord?></h3>
              <p>Total Transaksi</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
          </div>
        </div>