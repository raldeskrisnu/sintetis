<?php
include '../config.php';
if (isset($_SESSION['doku_user_id']) && $_SESSION['doku_user_id']!="") {
	include 'header.php';
	$page = isset($_GET['page']) ? strtolower($_GET['page']) : NULL;
	if (file_exists($page.'.php')) {
		echo "<script>console.log('$page');</script>";
		include $page.'.php';
	} 
	else {
		include 'home.php';
	}
	include 'footer.php';
} else {
	include 'login.php';
}
 mysql_close($conn);
?>