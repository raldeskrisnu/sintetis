<?php
include '../config.php';
if (isset($_SESSION['doku_user_id'])) {
	// remove all session variables
	unset($_SESSION['doku_user_id']);
	// destroy the session
	session_destroy(); 
	echo '<script type="text/javascript">
 		window.location = "login";
 		</script>';
	
}

mysql_close($conn);
?>