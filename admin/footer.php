      </div>
    </section>
  </div>
</div>
<script src="../asset/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../asset/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../asset/dist/js/adminlte.min.js"></script>
<script src="../asset/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../asset/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script type="text/javascript">
  $(function () {
    $('#list').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    });
    $("#gift_file").change(function() {
      readURL(this);
    });
    $('.textarea').wysihtml5()
  });
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  
  function transIDMerchant(id) {
    
    $("div.modal-body").html("<p>Loading ...</p>");
    $.post("../checkStatus.php",{transidmerchant: id}, function(msg) {
      var message = "";
      $.each(msg, function(key,val){
        message = message + "<strong>"+key+"</strong>";
        message = message + "<p class='text-muted'>"+val+"</p>";
        message = message + "<hr>";
      });
      $("div.modal-body").html(message);
    });
  }
</script>
<script  type="text/javascript">
  $(document).ready(function() {

    jQuery('#export-menu li').bind("click", function() {
      var target = $(this).attr('id');
      switch(target) {
      case 'export-to-excel' :
        $('#hidden-type').val(target);
        //alert($('#hidden-type').val());
        $('#export-form').submit();
        $('#hidden-type').val('');
      break
      case 'export-to-csv' :
        $('#hidden-type').val(target);
        //alert($('#hidden-type').val());
        $('#export-form').submit();
        $('#hidden-type').val('');
      break
}
});
    });
</script>
</body>
</html>