<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Donasi</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="asset/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="asset/dist/css/donation.css">
  
</head>
<body>
<div class="container">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" class="active" id="login-form-link">1.Donation</a>
							</div>
							<div class="col-xs-6">
								<a href="#" id="register-form-link">2.Pembayaran</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<!-- donation form -->
								<form id="login-form" action="#" method="post" role="form" style="display: block;">
									<div class="form-group">
										<center>
										<h2>Pilih Donasi</h2>
										</center>
									</div>

								<center>
									<div class="form-group">
										<div class="col-sm-4">
											<input type="radio" name="donatype" value="Individu"> Rp.50.000
										</div>
										<div class="col-sm-4">
											<input type="radio" name="donatype" value="Individu"> Rp.100.000
										</div>
										<div class="col-sm-4">
											<input type="radio" name="donatype" value="Individu"> Rp.150.000
										</div>
									</div>
								</center>
									
								<div class="form-group ex2">
										<center>
										<h2>Nominal Donasi Lainnya</h2>
										</center>
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Lanjutkan">
											</div>
										</div>
									</div>
								
								</form>
								<!-- closed form login -->

								<!-- register form -->
								<form id="register-form" action="#" method="post" role="form" style="display: none;">
									<div class="form-group">
										<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
									</div>
									<div class="form-group">
										<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="">
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group">
										<input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
											</div>
										</div>
									</div>
								</form>
								<!-- register form -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script src="asset/dist/js/jquery-1.11.1.min.js"></script>
<script src="asset/dist/js/bootstrap.min.js"></script>
<script>
$(function() {

$('#login-form-link').click(function(e) {
	$("#login-form").delay(100).fadeIn(100);
	 $("#register-form").fadeOut(100);
	$('#register-form-link').removeClass('active');
	$(this).addClass('active');
	e.preventDefault();
});
$('#register-form-link').click(function(e) {
	$("#register-form").delay(100).fadeIn(100);
	 $("#login-form").fadeOut(100);
	$('#login-form-link').removeClass('active');
	$(this).addClass('active');
	e.preventDefault();
});

});


</script>

</body>

</html>
