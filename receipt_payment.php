<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Donasi</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="asset/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="asset/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="asset/dist/css/donation.css">
  
</head>
<body>

<div class="container">
    <div class="row">
        <div class="container">
            <div class="row">
            	
                <div class="col-xs-6 col-sm-6 col-md-6">
                	
                    <addresslogo>
                        <strong>Jalan Amil Raya No 5</strong>
                        <br>
                        Pejaten Barat
                        <br>
                        Pasar minggu, Jakarta Selatan
                        <br>
                        <abbr title="Phone">Phone:</abbr> +62-21 720 8515
                        <br>
                        <abbr title="Phone">Fax:</abbr> +62-21 726 7815 
                    </address>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <?php
                       
                            $dokuDonaturId = $_SESSION["donationid"];
                           
                        ?>
                    <p>
                        <em>Tanggal: <?php echo date("F j, Y"); ?></em>
                    </p>
                    <p>
                        <em>ID Donasi: <?php echo $dokuDonaturId; ?></em>
                    </p>
                </div>
            </div>
            <div class="row">
            	
                <div class="text-center">
                    <h1>Info Donasi</h1>
                </div>
                </span>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Jenis Donasi</th>
                            <th>Nama Individu/Lembaga</th> 
                            <th>Email</th> 
                            <th>Kode Area</th> 
                            <th>Kota</th> 
                            <th>Negara</th> 
                            <th>Telepon</th>
                            <th>Nilai</th> 
                            <th>Appreciation Gift</th> 
                            <th>Metode Pembayaran</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <?php

                            $dokuDonaturId2 = $_SESSION["donationid"];
                            $sql = "SELECT a.doku_donatur_id,a.fullname,a.donatype,a.email,a.price,a.amount,a.product,a.payment_status,a.telp,a.country,a.city,a.areacode, a.method, b.gift_title FROM doku_donatur a LEFT JOIN doku_gift b ON b.doku_gift_id=a.doku_gift_id WHERE a.doku_donatur_id = '$dokuDonaturId2'";
                            $selectdb = mysql_select_db($dbname);
                            if($result = mysql_query($sql)){
                                if(mysql_num_rows($result) > 0){
                                    while($row = mysql_fetch_array($result)){
                                        $donationType = $row['product'];
                                        $resultDonation = $row['price'];
                                        $amount = $row['amount'];
                                        $name = $row['fullname'];
                                        $email = $row['email'];
                                        $kodearea = $row['areacode'];
                                        $city = $row['city'];
                                        $country = $row['country'];
                                        $telp = $row['telp'];
                                        $Appreciation = $row['telp'];
                                        $metode = $row['method'];
                                        $gift_title = $row['gift_title'];

                                    }
                                }
                                
                            }
                        ?>

                            <td class="col-md-3"><em><?php echo $donationType; ?></em></h4></td>
                            <td class="col-md-3"><em><?php echo $name; ?></em></h4></td>
                            <td class="col-md-3"><?php echo $email; ?></td>
                            <td class="col-md-9"><em><?php echo $kodearea; ?></em></h4></td>
                            <td class="col-md-3"><?php echo $city; ?></td>
                            <td class="col-md-9"><em><?php echo $country; ?></em></h4></td>
                            <td class="col-md-3"><?php echo $telp; ?></td>
                            <td class="col-md-9"><em>
                            <?php 
                            echo "Rp " . number_format($resultDonation,0,',','.');
                           // 
                            ?>
                            </em></h4></td>
                            <td class="col-md-3 text-center"><?php echo $gift_title; ?></td>
                            <td class="col-md-3 text-center"><?php echo $metode; ?></td>
                        </tr>

                    </tbody>
                </table>
                <a href="confirmation" class="btn btn-primary btn-lg btn-block">Konfirmasi</a></td>
               <!--  <button type="button" class="btn btn-success btn-lg btn-block">
                    Konfirmasi  
                </button></td> -->

                <div class="text-center">
                    <h2>Nomor Rekening</h2>
                    <h4>Bank Mandiri</h4>
                    <h4>102 000 5836 611 a/n Transparency International Indonesia</h4>
                    <h5>Harap melakukan pembayaran donasi dalam waktu 3 Jam.
                        <br>
                <br>
                <br>

                <h5>Catatan: Apabila Email tidak masuk kedalam inbox, silahkan cek email spam</h5>
                </div>
                
            </div>
        </div>


    </div>

<script src="asset/dist/js/jquery-1.11.1.min.js"></script>
<script src="asset/dist/js/bootstrap.min.js"></script>

</body>

</html>
