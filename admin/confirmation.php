<div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Konfirmasi Donasi</h3>
            </div>
            <div class="box-body">
              <!-- <a class="btn btn-app btn-success" href="index.php?page=gift_new">
                <i class="fa fa-asterisk"></i> Buat Baru
              </a> -->
              <table id="list" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>ID Donasi</th>
                    <th>Nama Donatur</th>
                    <th>Organisasi</th>
                    <th>Email</th>
                    <th>Jumlah Donasi</th>
                    <th>Jenis Donasi</th>
                    <th>Payment Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                
                <?php
                  $selectdb = mysql_select_db($dbname);
                  $no = 1;
                  $sql = "SELECT a.doku_donatur_id,a.fullname,a.donatype,a.email,a.price,a.amount,a.product,a.payment_status,a.telp,a.country,a.city,a.areacode, a.bukti_transfer, b.gift_title FROM doku_donatur a INNER JOIN doku_gift b ON b.doku_gift_id=a.doku_gift_id";
                  if($result = @mysql_query($sql)){
                      if(mysql_num_rows($result) > 0){
                        while($row = mysql_fetch_array($result)){
                  ?>
                        <tr>
                           <td><?php echo $no++ ?></td>
                            <td><?php echo $row['doku_donatur_id'] ?></td>
                           <td><?php echo $row['fullname'] ?></td>
                           <td><?php echo $row['donatype'] ?></td>
                           <td><?php echo $row['email'] ?></td>
                           <td><?php echo "Rp " . number_format($row['price'],0,'.','.');  ?></td>
                           <td><?php echo $row['product'] ?></td>
                           <td><?php echo $row['payment_status'] ?></td>
                           <td width="140x">
                            <center>
                               <button class="btn btn-primary" data-toggle="modal" data-target="#<?php echo $row['doku_donatur_id'] ?>">Konfirmasi</button>
                             </center>
                           </td>
                         </tr>

                         <!-- Dialog Modal Konfirmasi -->
                           <div class="modal fade" id="<?php echo $row['doku_donatur_id'] ?>" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
                              <form action="confirmationProcess" method="post" id="clickform" class="form-horizontal" enctype="multipart/form-data">
                                 <div class="modal-dialog">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                          <h4 class="modal-title" id="modal" ><font face="Comic Sans MS"><?php echo "Konfirmasi"; ?> </font></h4>
                                       </div>
                                       <div class="modal-body">
                                            
                                            <div class="form-group">
                                             <label class="col-md-3 control-label" for="idDonasi">ID Donasi </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="idonatur" name="idonatur" value="<?php 
                                                $testing = $row['doku_donatur_id'];
                                                echo $testing; ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>
                                          <p>&nbsp;</p>

                                            <!-- <div class="form-group">
                                             <label class="col-md-3 control-label" for="idDonasi">ID Donasi </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="idDonasi" name="idDonasi" value="<?php echo $row['doku_donatur_id'] ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>
                                          <p>&nbsp;</p>

                                          <div class="form-group">
                                             <label class="col-md-3 control-label" for="namatopic">Nama Individu / Lembaga </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['fullname'] ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>

                                          <p>&nbsp;</p>
                                          <div class="form-group">
                                             <label class="col-md-3 control-label" for="namatopic">Jenis </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['donatype'] ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>

                                          <p>&nbsp;</p>
                                          <div class="form-group">
                                             <label class="col-md-3 control-label" for="namatopic">Email </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['email'] ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>

                                          <p>&nbsp;</p>
                                          <div class="form-group">
                                             <label class="col-md-3 control-label" for="namatopic">Jumlah Donasi </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="namatopic" name="namatopic" value="<?php echo "Rp " . number_format($row['price'],0,'.','.'); ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>

                                          <p>&nbsp;</p> 
                                          <div class="form-group">
                                             <label class="col-md-3 control-label" for="namatopic">Nomor Telp </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['telp'] ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>

                                          <p>&nbsp;</p> 
                                          <div class="form-group">
                                             <label class="col-md-3 control-label" for="namatopic">Kode Area </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['areacode'] ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>

                                          <p>&nbsp;</p> 
                                          <div class="form-group">
                                             <label class="col-md-3 control-label" for="namatopic">Kota </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['city'] ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>

                                           <p>&nbsp;</p> 
                                          <div class="form-group">
                                             <label class="col-md-3 control-label" for="namatopic">Negara </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['country'] ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>

                                          <p>&nbsp;</p> 
                                          <div class="form-group">
                                             <label class="col-md-3 control-label" for="namatopic">Appreciation Gift </label>  
                                             <div class="col-md-9">
                                                <input type="text" id="namatopic" name="namatopic" value="<?php echo $row['gift_title'] ?>" class="form-control input-md" readonly>
                                             </div>
                                          </div>
                                          <p>&nbsp;</p>  -->

                                          <div class="form-group">
                                                <label class="col-xs-3 control-label">Pilih Status</label>
                                                    <div class="col-xs-5 selectContainer">
                                                        <select class="form-control" name="option" id="option">
                                                              <option value="" selected="true" disabled="disabled">Status</option>
                                                              <option value="SUCCESS">Success</option>
                                                              <option value="PENDING">Pending</option>
                                                              <option value="REJECT">Reject</option>
                                                        </select>
                                                    </div>
                                          </div> 
                                          <p>&nbsp;</p> 
                                  <p>&nbsp;</p> 
                                          <div class="form-group">
                                                <label class="col-xs-3 control-label">Lihat Bukti Transfer</label>
                                                <div class="col-md-9">
                                                   
                                                    <a href="../asset/upload/transfer/<?php echo $row['bukti_transfer']; ?>" target"_blank">Lihat Gambar</a>
                                                  </div>
                                          </div> 
                                          <p>&nbsp;</p> 

                                          <div class="form-group">
                                            <br>
                                              <input type="submit" class="btn btn-primary btn-block" value="Submit">
                                          </div>
                                          <p>&nbsp;</p> 

                                       </div>
                                       <div class="modal-footer">
                                          <p> © Copyright 2018. sintesis.ti.or.id</p>
                                       </div>
                                    </div>
                                 </div>
                                 </form>
                           </div>
                        <?php }
                      } else {
                        echo "0 results";
                      }
                  }
                
                  ?>
                  
                <tfoot>
                  
                </tfoot>
              </table>
            </div>
          </div>
        </div>