-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 06, 2018 at 12:12 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `donasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `doku_donatur`
--

CREATE TABLE `doku_donatur` (
  `doku_donatur_id` int(11) UNSIGNED NOT NULL,
  `donatype` varchar(60) NOT NULL,
  `fullname` varchar(60) NOT NULL,
  `areacode` varchar(10) NOT NULL,
  `telp` varchar(60) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(60) NOT NULL,
  `country` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `price` float NOT NULL DEFAULT '0',
  `amount` float DEFAULT '0',
  `method` varchar(32) NOT NULL,
  `cardno` varchar(60) DEFAULT NULL,
  `cardcode` varchar(10) DEFAULT NULL,
  `expired` varchar(60) DEFAULT NULL,
  `expiredyear` varchar(60) DEFAULT NULL,
  `product` varchar(60) DEFAULT NULL,
  `currency` int(5) NOT NULL,
  `approvalcode` varchar(60) DEFAULT NULL,
  `bank` varchar(60) DEFAULT NULL,
  `brand` varchar(60) DEFAULT NULL,
  `chname` varchar(60) DEFAULT NULL,
  `edustatus` varchar(60) DEFAULT NULL,
  `liability` varchar(60) DEFAULT NULL,
  `mcn` varchar(60) DEFAULT NULL,
  `paymentchannel` varchar(60) DEFAULT NULL,
  `paymentcode` varchar(60) DEFAULT NULL,
  `paymentdatetime` varchar(60) DEFAULT NULL,
  `purchasecurrency` varchar(60) DEFAULT NULL,
  `responsecode` varchar(60) DEFAULT NULL,
  `resultmsg` varchar(60) DEFAULT NULL,
  `sessionid` varchar(60) DEFAULT NULL,
  `statustype` varchar(60) DEFAULT NULL,
  `threedsecurestatus` varchar(60) DEFAULT NULL,
  `verifyid` varchar(60) DEFAULT NULL,
  `verifyscore` varchar(60) DEFAULT NULL,
  `verifystatus` varchar(60) DEFAULT NULL,
  `statuscode` varchar(60) DEFAULT NULL,
  `doku_gift_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doku_donatur`
--

INSERT INTO `doku_donatur` (`doku_donatur_id`, `donatype`, `fullname`, `areacode`, `telp`, `address`, `city`, `country`, `email`, `price`, `amount`, `method`, `cardno`, `cardcode`, `expired`, `expiredyear`, `product`, `currency`, `approvalcode`, `bank`, `brand`, `chname`, `edustatus`, `liability`, `mcn`, `paymentchannel`, `paymentcode`, `paymentdatetime`, `purchasecurrency`, `responsecode`, `resultmsg`, `sessionid`, `statustype`, `threedsecurestatus`, `verifyid`, `verifyscore`, `verifystatus`, `statuscode`, `doku_gift_id`) VALUES
(1, 'Lembaga', 'Suliyarto Karta', '812', '081289718075', 'Komplek DPR 2 Blok D/56 RT 011/002\r\nJl. Masjid Al Ikhlas II - Meruya Selatan', 'Jakarta Barat', 'ID', 'suliyarto@yahoo.co.id', 50000, 1500, 'debit_credit', '5548812572487581', '271', '09', '20', 'Donasi Rutin', 360, '072969', 'Bank Mandiri', 'VISA', 'Suliyarto Karta', 'NA', 'CUSTOMER', '411111******1111', '15', '', '20180311230438', '360', '0000', 'SUCCESS', 'kkgj2t9vgch6gu36lqj8kjdqi7', 'P', 'TRUE', '', '-1', 'NA', '0000', NULL),
(2, 'Individu', 'Wahyu', '62', '8157992747', 'Fafa', 'fafa', 'ID', 'Wahyu@ti.or.id', 50000, 0, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307135654', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Lembaga', 'Suliyarto Karta', '812', '081289718075', 'Komplek DPR 2 Blok D/56 RT 011/002\r\nJl. Masjid Al Ikhlas II - Meruya Selatan', 'Jakarta Barat', 'ID', 'suliyarto@yahoo.co.id', 0, 15000, 'debit_credit', '4111Â­1111Â­1111Â­1111', '869', '04', '20', 'Donasi Rutin', 360, '', 'Bank Mandiri', 'VISA', 'Suliyarto Karta', 'NA', 'NA', '411111******1111', '15', '', '20180307144314', '360', '003D', 'FAILED', 's810k4ae5i7qar75kjuffm2qs6', 'P', 'TRUE', '', '-1', 'NA', '003D', NULL),
(4, 'Individu', 'Suliyarto Karta', '812', '081289718075', 'Komplek DPR 2 Blok D/56 RT 011/002\r\nJl. Masjid Al Ikhlas II - Meruya Selatan', 'Jakarta Barat', 'ID', 'suliyarto@yahoo.co.id', 0, 15000, 'debit_credit', 'Â 5426Â­4000Â­3010Â­8754', '869', '04', '20', 'Donasi Rutin', 360, '840765', 'Permata', 'MASTERCARD', 'Suliyarto Karta', 'NA', 'CUSTOMER', '542640******8754', '15', '', '20180307044653', '360', '0000', 'SUCCESS', 's810k4ae5i7qar75kjuffm2qs6', 'P', 'TRUE', '', '-1', 'NA', '5508', NULL),
(5, 'Individu', 'Suliyarto Karta', '812', '081289718075', 'Komplek DPR 2 Blok D/56 RT 011/002\r\nJl. Masjid Al Ikhlas II - Meruya Selatan', 'Jakarta Barat', 'ID', 'suliyarto@yahoo.co.id', 0, 0, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, '', '', NULL, NULL, NULL, NULL, '', '35', '', '', NULL, '5516', 'TRANSACTION_NOT_FOUND', 'q7rft2tiet10tg46onamilen50', NULL, NULL, '', '-1', 'NA', '5511', NULL),
(6, 'Individu', 'Suliyarto Karta', '812', '081289718075', 'Komplek DPR 2 Blok D/56 RT 011/002\r\nJl. Masjid Al Ikhlas II - Meruya Selatan', 'Jakarta Barat', 'ID', 'suliyarto@yahoo.co.id', 0, 12300, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307145641', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Individu', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307153335', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Individu', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '35', NULL, '20180307153449', NULL, '5516', 'TRANSACTION_NOT_FOUND', 'q7rft2tiet10tg46onamilen50', NULL, NULL, NULL, '-1', 'NA', NULL, NULL),
(9, 'Individu', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'transfer', '', '', '03', '18', 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307153712', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Lembaga', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'debit_credit', '4097662860888562', '530', '08', '27', 'Donasi Program', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '01', '', '20180307153904', NULL, NULL, NULL, '908g7p8noedbi3pojmo00vlq21', NULL, NULL, NULL, NULL, NULL, '5510', NULL),
(11, '', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307154729', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '35', NULL, '20180307155822', NULL, NULL, NULL, '908g7p8noedbi3pojmo00vlq21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'Individu', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '35', NULL, '20180307162728', NULL, NULL, NULL, '908g7p8noedbi3pojmo00vlq21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Individu', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '35', NULL, '20180307162825', NULL, NULL, NULL, '908g7p8noedbi3pojmo00vlq21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Individu', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'debit_credit', '4111111111111111', '869', '04', '20', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', '', '20180307164411', NULL, NULL, NULL, '908g7p8noedbi3pojmo00vlq21', NULL, NULL, NULL, NULL, NULL, '0000', NULL),
(16, 'Individu', 'Wahyudi', '62', '8157992747', 'Wahyudi', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307164744', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Individu', 'Wahyudi', '62', '8157992747', 'Wahyud', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307165419', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'Individu', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', NULL, '20180307173956', NULL, NULL, NULL, '908g7p8noedbi3pojmo00vlq21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'Individu', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', NULL, '20180307174245', NULL, NULL, NULL, '908g7p8noedbi3pojmo00vlq21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'Individu', 'Wahyudi', '62', '8157992747', 'Jalan Amil No. 5', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', NULL, '20180307174422', NULL, NULL, NULL, '908g7p8noedbi3pojmo00vlq21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'Individu', 'Wahyudi', '62', '8157992747', 'Kbagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307211012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'Individu', 'Wahyudi', '62', '8157992747', 'Kbagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307211115', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'Individu', 'Wahyudi', '62', '8157992747', 'Kbagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307211125', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'Individu', 'Wahyudi', '62', '8157992747', 'Kbagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307211256', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307211532', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'Individu', 'Eahyu', '56', '852894', 'Kabgaus', 'Jakata', 'ID', 'wahyudo@toriro.com', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307212046', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'Individu', 'Eahyu', '56', '852894', 'Kabgaus', 'Jakata', 'ID', 'wahyudo@toriro.com', 50000, 0, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307212437', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'Individu', 'TII', '021', '7208515', 'Jakarta', 'Jakarta Selatan', 'ID', 'nurfajrin7@gmail.com', 50000, 50000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, 'VISA', 'TII', 'NA', NULL, '411111******1111', '15', NULL, '20180307212839', '360', '5519', 'FAILED', '6tni7cbgbngk5ubqgdvt0s8tn5', 'P', NULL, NULL, '-1', 'NA', NULL, NULL),
(29, 'Lembaga', 'TII', '021', '7208515', 'Jakarta', 'Jakarta selatan', 'ID', 'nurfajrin7@gmail.com', 50000, 50000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '35', '8888870811620033', '20180307213932', NULL, NULL, NULL, '6tni7cbgbngk5ubqgdvt0s8tn5', NULL, NULL, NULL, NULL, NULL, '5511', NULL),
(30, 'Individu', 'Wahyud', '62', '8157992747', 'Wahyudi', 'Jakarta ', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307214445', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'Lembaga', 'Tii', '021', '7208515', 'Jakarta', 'Jakarta Selatan', 'ID', 'nurfajrin7@gmail.com', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180307214631', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'Individu', 'Dea Fauzia Akhmad', '62', '81288742630', 'Jl. Masjid Al Ikhlas II - Komp. DPR 2 Blok D56 RT 11/02 - Meruya Selatan - 11650', 'Jakarta Barat', 'ID', 'deafauzia91@gmail.com', 0, 12300, '', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, 'MASTERCARD', 'Dea Fauzia Akhmad', 'NA', NULL, '542640******8754', '15', NULL, '20180307224714', '360', '5519', 'FAILED', 's810k4ae5i7qar75kjuffm2qs6', 'P', NULL, NULL, '-1', 'NA', NULL, NULL),
(33, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180308074505', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180308074540', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180308074635', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', NULL, '20180308102300', NULL, NULL, NULL, '908g7p8noedbi3pojmo00vlq21', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 'Individu', 'Wahyudi', '62', '8157992747', 'Kevagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180308102733', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'Individu', 'Wahyudi', '62', '8157992747', 'Kevagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180308102748', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'Individu', 'Wahyudi', '62', '8157992747', 'Kevagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180308102759', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'Individu', 'Wahyudi', '62', '8157992747', 'Kevagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180308102819', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'Individu', 'Wahyudi', '62', '8157992747', 'Kevagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180308102830', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'Individu', 'Wahyudi', '62', '8157992747', 'Kevagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180308143029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, '', '', 'VISA', 'Wahyudi', 'NA', '', '411111******1111', '15', '', '', '360', '5519', 'FAILED', '908g7p8noedbi3pojmo00vlq21', 'P', '', '', '-1', 'NA', NULL, NULL),
(44, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'transfer', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '01', '', '20180308163344', NULL, NULL, NULL, '908g7p8noedbi3pojmo00vlq21', NULL, NULL, NULL, NULL, NULL, '5510', NULL),
(45, 'Individu', 'Wahyudi', '62', '8157992747', 'Wahyud', 'wa', 'ID', 'wahyudi@ti.or.id', 50000, 50000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, '030474', 'Bank Mandiri', 'VISA', 'Wahyudi', 'NA', 'CUSTOMER', '411111******1111', '15', NULL, '20180309004252', '360', '0000', 'SUCCESS', '908g7p8noedbi3pojmo00vlq21', 'P', 'TRUE', NULL, '-1', 'NA', NULL, NULL),
(46, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 2000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180311181021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 0, 2000, 'doku', '', '', '03', '18', 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180311181043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta', 'ID', 'wahyudi@ti.or.id', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180312122152', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 'Individu', 'Wahyudi', '62', '8157992747', 'KEBAGUSAN', 'Jakarta Selatan', 'ID', 'wahyudi.thohary@gmail.com', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180312123746', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 'Individu', 'Wahyudi', '62', '8157992747', 'KEBAGUSAN', 'Jakarta Selatan', 'ID', 'wahyudi.thohary@gmail.com', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', '', '20180312130022', NULL, NULL, NULL, 'oft38550k4n7no6iigu0l42iq4', NULL, NULL, NULL, NULL, NULL, '0000', NULL),
(51, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta ', 'ID', 'wahyudi@ti.or.id', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Program', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180312185400', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta ', 'ID', 'wahyudi@ti.or.id', 50000, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Program', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180312185420', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180312192112', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'Individu', 'Nur', '021', '7208515', 'Jakarta', 'Jakart', 'ID', 'nurfajrin7@gmail.com', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180313104953', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Program', 360, NULL, NULL, 'VISA', 'Wahyudi', 'NA', NULL, '411111******1111', '15', NULL, '20180313111243', '360', '5519', 'FAILED', '7l2ohqd6narnltej7bgmrrtqn5', 'P', NULL, NULL, '-1', 'NA', NULL, NULL),
(56, 'Individu', 'nur', '021', '22792807', 'Jakarta', 'Jakarta Selatan', 'ID', 'nfajrin@ti.or.id', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180314112756', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'Individu', 'Wahyudi', '62', '8157992747', 'Wahyudi', 'Jakarta', 'ID', 'wahyudi@ti.or.id', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180315195746', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'Individu', 'Wahyudi', '62', '8157992747', 'Wahyudi', 'Jakarta', 'ID', 'wahyudi@ti.or.id', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180315200006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180316112002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 0, 5000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '35', NULL, '20180316183042', NULL, NULL, NULL, '2nmc4n9glvng67f93asoruuhf6', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 0, 5000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', NULL, '20180316184826', NULL, NULL, NULL, '2nmc4n9glvng67f93asoruuhf6', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'Individu', 'Charles', '021', '122435363', 'baghdad', '021', 'ID', 'cahrels@ccc.com', 0, 2000000000, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '01', '', '20180316190150', NULL, NULL, NULL, 'n843f4g484abg8c61a1050v091', NULL, NULL, NULL, NULL, NULL, '5510', NULL),
(63, 'Individu', 'ani', '021', '12345', 'fsfs', 'sdsd', 'ID', 'sdasd@xxx.com', 0, 200, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, 'NA', NULL, 'wahyudi@ti.or.id', '04', NULL, '20180316190325', '360', '5511', 'UNPAID', 'n843f4g484abg8c61a1050v091', 'P', NULL, NULL, '-1', 'NA', NULL, NULL),
(64, 'Individu', 'Wahyudi', '62', '8157992747', 'Jakarta Selatan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180317114711', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 'Individu', 'Wahyudi', '62', '8157992747', 'Jakarta Selatan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180317114728', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'Individu', 'Wahyudi', '62', '8157992747', 'Jakarta Selatan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180317114746', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180319125619', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 0, 10000, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180319211320', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 0, 10000, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180319230443', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi.thohary@gmail.com', 50000, 50000, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', '', '20180320102126', NULL, NULL, NULL, '7pirtndmvl7jsvaekkd9jhi031', NULL, NULL, NULL, NULL, NULL, '0000', NULL),
(71, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Program', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180321114506', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'Lembaga', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '01', '', '20180322105456', NULL, NULL, NULL, 'lgpumciq48rm5no98lnlbjvrk4', NULL, NULL, NULL, NULL, NULL, '5510', NULL),
(73, '', 'Suliyarto Karta', '812', '081289718075', 'Komplek DPR 2 Blok D/56 RT 011/002\r\nJl. Masjid Al Ikhlas II - Meruya Selatan', 'Jakarta Barat', 'ID', 'suliyarto@yahoo.co.id', 0, 18500, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, 'NA', NULL, '8888870811620060', '35', '8888870811620060', '20180326055837', '360', '5511', 'UNPAID', 'q7rft2tiet10tg46onamilen50', 'P', NULL, NULL, '-1', 'NA', '5511', 1),
(74, 'Individu', 'Wahyu', '62', '08125777', 'Kebagys', 'Jajarta', 'ID', 'www@gmi.com', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180326080806', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(75, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Kebagusan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', NULL, '20180326104644', NULL, NULL, NULL, 'g81ugttmv456g5lbcqu8djsv72', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(76, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '35', NULL, '20180326105247', NULL, NULL, NULL, 'g81ugttmv456g5lbcqu8djsv72', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(77, 'Lembaga', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180326120038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(78, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 0, 2000, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180326120237', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(79, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 0, 10000, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', NULL, '20180326121036', NULL, NULL, NULL, '2clqdabqnnif2qj9r2khr3c6l7', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(80, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta', 'ID', 'wahyudi@ti.or.id', 50000, 50000, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', '', '20180326121816', NULL, NULL, NULL, '589p598f615fot65dnodo03tm2', NULL, NULL, NULL, NULL, NULL, '0098', 1),
(81, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180329143230', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19),
(82, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180329145712', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 19),
(83, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusab', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180331191554', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(84, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Sekali', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180402173644', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(85, 'Lembaga', 'asd', '1231', 'asda', 'jakarta', 'jakarta', 'ID', 'asdsad@gmail.com', 150000, 150000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '01', '', '20180402192426', NULL, NULL, NULL, '43belnni1qvavuakg1nti6eu57', NULL, NULL, NULL, NULL, NULL, '5510', 19),
(86, '', 'kj', '1231', '54584815', 'asda', 'jakarta', 'ID', 'asdasd@gmail.com', 150000, 54894600, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180402193042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(87, 'Lembaga', 'ss', '12312', '213123', 'asda', 'sdas', 'ID', 'sdsa@gmail.com', 150000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180403103836', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(88, 'Individu', 'ss', '12312', '213123', 'asda', 'sdas', 'ID', 'sdsa@gmail.com', 150000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180403105141', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(89, 'Individu', 'ss', '12312', '213123', 'asda', 'sdas', 'ID', 'sdsa@gmail.com', 150000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180403105148', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(90, 'Individu', 'ss', '12312', '213123', 'asda', 'sdas', 'ID', 'sdsa@gmail.com', 150000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180403105157', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(91, 'Individu', 'ss', '12312', '213123', 'asda', 'sdas', 'ID', 'sdsa@gmail.com', 150000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180403105201', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(92, 'Individu', 'ss', '12312', '213123', 'asda', 'sdas', 'ID', 'sdsa@gmail.com', 150000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180403105213', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(93, 'Individu', 'Wahyudi', '62', '8157992747', 'Kebagusan', 'Jakarta Selatan', 'ID', 'wahyudi@ti.or.id', 50000, 50000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '15', NULL, '20180403113633', NULL, NULL, NULL, 've58ou1k7j3t7r3hdn8sshk3v7', NULL, NULL, NULL, NULL, NULL, NULL, 17),
(94, 'Lembaga', 'sdasd', '123', 'asd', 'asd', 'asda', 'ID', 'asda@gmail.com', 150000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180403174303', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(95, 'Individu', 'Ani', '021', '1234567891', 'jakarta', 'jakarta selatan', 'ID', 'info@ti.or.id', 0, 3000, '', NULL, NULL, NULL, NULL, 'Donasi Program', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180405203841', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(96, 'Individu', 'dgfksjdgh', '021', '112435646', 'dglfkg', 'dfskjg', 'ID', 'smdjh@djflsdg.com', 0, 3000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '35', NULL, '20180405212235', NULL, NULL, NULL, '6o412fna415vgt9caot3llnim6', NULL, NULL, NULL, NULL, NULL, NULL, 17),
(97, 'Individu', 'dgfksjdgh', '021', '112435646', 'dglfkg', 'dfskjg', 'ID', 'smdjh@djflsdg.com', 0, 3000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180405212332', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(98, 'Individu', 'dgfksjdgh', '021', '112435646', 'dglfkg', 'dfskjg', 'ID', 'smdjh@djflsdg.com', 0, 3000, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180405212419', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17),
(99, 'Individu', 'adsasd', '1231', 'asdadasdad1231', '2131asd', 'asdas', 'ID', 'ada@gmail.com', 50000, 0, '', NULL, NULL, NULL, NULL, 'Donasi Rutin', 360, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20180406163601', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 17);

-- --------------------------------------------------------

--
-- Table structure for table `doku_gift`
--

CREATE TABLE `doku_gift` (
  `doku_gift_id` int(11) NOT NULL,
  `gift_title` varchar(32) NOT NULL,
  `gift_file` varchar(60) DEFAULT NULL,
  `gift_message` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedby` int(11) NOT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doku_gift`
--

INSERT INTO `doku_gift` (`doku_gift_id`, `gift_title`, `gift_file`, `gift_message`, `created`, `createdby`, `updated`, `updatedby`, `isactive`) VALUES
(1, 'Alat Musik Tradisional', 'GIFT-201803260546455.png', '<p>Alat musik khas dari bagian <b><i>Nusantara</i></b></p>', '2018-03-25 22:46:45', 0, '2018-03-25 22:46:45', 0, 0),
(2, 'Kaos kampanye antikorupsi', 'GIFT-201803261232486.jpg', '<p>Dapatkan kaos cantik setiap donasi sebesa Rp. 150.000,-<br></p>', '2018-03-26 05:32:48', 0, '2018-03-26 05:32:48', 0, 0),
(3, 'Lorem Ipsum', 'GIFT-201803261439217.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. <br></p><p></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. <br><br></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. <br><br></p><br><p></p>', '2018-03-26 07:39:21', 0, '2018-03-26 07:39:21', 0, 0),
(4, 'Lorem Ipsum', 'GIFT-201803261440148.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec \r\nodio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla \r\nquis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent \r\nmauris. Fusce nec tellus sed augue semper porta. Mauris massa. \r\nVestibulum lacinia arcu eget nulla. <br></p>', '2018-03-26 07:40:14', 0, '2018-03-26 07:40:14', 0, 0),
(5, 'Lorem Ipsum', 'GIFT-201803261448029.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec \r\nodio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla \r\nquis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent \r\nmauris. Fusce nec tellus sed augue semper porta. Mauris massa. \r\nVestibulum lacinia arcu eget nulla. <br></p>', '2018-03-26 07:48:02', 0, '2018-03-26 07:48:02', 0, 0),
(6, 'Lorem Ipsum', 'GIFT-2018032614481810.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec \r\nodio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla \r\nquis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent \r\nmauris. Fusce nec tellus sed augue semper porta. Mauris massa. \r\nVestibulum lacinia arcu eget nulla. <br></p>', '2018-03-26 07:48:18', 0, '2018-03-26 07:48:18', 0, 0),
(7, 'Lorem Ipsum', 'GIFT-2018032614483711.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec \r\nodio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla \r\nquis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent \r\nmauris. Fusce nec tellus sed augue semper porta. Mauris massa. \r\nVestibulum lacinia arcu eget nulla. <br></p>', '2018-03-26 07:48:37', 0, '2018-03-26 07:48:37', 0, 0),
(8, 'Lorem Ipsum', 'GIFT-2018032713514412.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec \r\nodio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla \r\nquis sem at nibh elementum imperdiet. <br></p>', '2018-03-27 06:51:44', 0, '2018-03-27 06:51:44', 0, 0),
(9, 'Lorem Ipsum', 'GIFT-2018032713542312.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.<br></p>', '2018-03-27 06:54:23', 0, '2018-03-27 06:54:23', 0, 0),
(10, 'Lorem Ipsum', 'GIFT-2018032713544112.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.<b></b><br></p><br>', '2018-03-27 06:54:41', 0, '2018-03-27 06:54:41', 0, 0),
(11, 'Lorem Ipsum', 'GIFT-2018032713552112.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi.<br></p>', '2018-03-27 06:55:21', 0, '2018-03-27 06:55:21', 0, 0),
(12, 'Lorem Ipsum', 'GIFT-2018032713561412.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi.<br></p>', '2018-03-27 06:56:14', 0, '2018-03-27 06:56:14', 0, 0),
(13, 'Lorem Ipsum', 'GIFT-2018032713562512.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi.<br></p>', '2018-03-27 06:56:25', 0, '2018-03-27 06:56:25', 0, 0),
(14, 'Lorem Ipsum', 'GIFT-2018032713563512.', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi.<br></p>', '2018-03-27 06:56:35', 0, '2018-03-27 06:56:35', 0, 0),
(15, 'Kaos Pintar Lawan Korupsi', 'GIFT-2018032718113612.jpeg', '<p>\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi.\r\n\r\n<br></p>', '2018-03-27 11:11:36', 0, '2018-03-27 11:11:36', 0, 0),
(16, 'Kaos Pintar Lawan Korupsi', 'GIFT-2018032718565713.png', '<p>\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi.\r\n\r\n<br></p>', '2018-03-27 11:56:57', 0, '2018-03-27 11:56:57', 0, 0),
(17, 'Kaos Pintar Lawan Korupsi', 'GIFT-2018032719110414.png', '<p>\r\n\r\nTahukah, Kamu? Bahwa Korupsi Merupakan Faktor Yang Paling Menghambat Kemudahan Berusaha di Indonesia. Korupsi Mengakibatkan Ekonomi Berbiaya Tinggi. Karena Korupsi Pengusaha Dapat Me-mark-up harga buku sekolah adikmu yang semula seharga Rp 1000, menjadi 10 kali lipatnya. Jadi, masih pingin korupsi menghambat akses pendidikan di Indonesia. Pakai Kaos ini sekarang. Dan, sampaikan pesan antikorupsi ini kepada orang di sekitarmu. \r\n\r\n<br></p>', '2018-03-27 12:11:04', 0, '2018-03-27 12:11:04', 0, 1),
(18, 'Kaos Pintar Lawan Korupsi', 'GIFT-2018032719163115.', '<p><img alt=\"\" src=\"http://sintesis.ti.or.id\"><br></p>', '2018-03-27 12:16:31', 0, '2018-03-27 12:16:31', 0, 0),
(19, 'Kaos Pintar Lawan Korupsi', 'GIFT-2018032719175815.png', '<p>\r\n\r\nTahukah, Kamu? Bahwa Korupsi Merupakan Faktor Yang Paling Menghambat Kemudahan Berusaha di Indonesia. Korupsi Mengakibatkan Ekonomi Berbiaya Tinggi. Karena Korupsi Pengusaha Dapat Me-mark-up harga buku sekolah adikmu yang semula seharga Rp 1000, menjadi 10 kali lipatnya. Jadi, masih pingin korupsi menghambat akses pendidikan di Indonesia. Pakai Kaos ini sekarang. Dan, sampaikan pesan antikorupsi ini kepada orang di sekitarmu. \r\n\r\n<br></p>', '2018-03-27 12:17:58', 0, '2018-03-27 12:17:58', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `doku_log`
--

CREATE TABLE `doku_log` (
  `id` int(10) NOT NULL,
  `filename` varchar(60) NOT NULL,
  `message` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doku_log`
--

INSERT INTO `doku_log` (`id`, `filename`, `message`, `created`) VALUES
(1, 'DOKU_Redirect', 'TRANSIDMERCHANT:72; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:01; PURCHASEAMOUNT:50000.00; STATUSCODE:5510; PAYMENTCODE:; AMOUNT:50000.00; WORDS:062efb52aed71d4e5d2862a5d5e93a36ed858b6e; SESSIONID:lgpumciq48rm5no98lnlbjvrk4; ', '2018-03-22 03:55:15'),
(2, 'DOKU_Redirect', 'TRANSIDMERCHANT:4; PURCHASECURRENCY:360; CURRENCY:360; STATUSCODE:5508; PAYMENTCODE:; AMOUNT:1500.00; WORDS:493d0944d800a7b827e175867b650f4c411eda2b; SESSIONID:kkgj2t9vgch6gu36lqj8kjdqi7; ', '2018-03-25 16:49:03'),
(3, 'DOKU_Identify', 'TRANSIDMERCHANT:73; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:35; PAYMENTCODE:8888870811620060; AMOUNT:18500.00; SESSIONID:q7rft2tiet10tg46onamilen50; ', '2018-03-25 22:58:52'),
(4, 'DOKU_Redirect', 'TRANSIDMERCHANT:73; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:35; STATUSCODE:5511; AMOUNT:18500.00; PAYMENTCODE:8888870811620060; WORDS:abee9cbbbd950738eb660cadf71b1d1fb00d4cd4; SESSIONID:q7rft2tiet10tg46onamilen50; ', '2018-03-25 22:59:01'),
(5, 'DOKU_Identify', 'TRANSIDMERCHANT:75; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:15; PAYMENTCODE:; AMOUNT:50000.00; SESSIONID:g81ugttmv456g5lbcqu8djsv72; ', '2018-03-26 03:49:45'),
(6, 'DOKU_Identify', 'TRANSIDMERCHANT:76; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:35; PAYMENTCODE:8888870811620062; AMOUNT:50000.00; SESSIONID:g81ugttmv456g5lbcqu8djsv72; ', '2018-03-26 03:53:15'),
(7, 'DOKU_Identify', 'TRANSIDMERCHANT:79; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:15; PAYMENTCODE:; AMOUNT:10000.00; SESSIONID:2clqdabqnnif2qj9r2khr3c6l7; ', '2018-03-26 05:12:24'),
(8, 'DOKU_Identify', 'TRANSIDMERCHANT:80; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:15; PAYMENTCODE:; AMOUNT:50000.00; SESSIONID:589p598f615fot65dnodo03tm2; ', '2018-03-26 05:22:18'),
(9, 'DOKU_Redirect', 'TRANSIDMERCHANT:80; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:15; STATUSCODE:0098; AMOUNT:50000.00; PAYMENTCODE:; WORDS:956e9899be87eb8642c96d8622a74c568337477b; SESSIONID:589p598f615fot65dnodo03tm2; ', '2018-03-26 05:22:40'),
(10, 'DOKU_Redirect', '', '2018-03-29 08:02:58'),
(11, 'DOKU_Redirect', 'TRANSIDMERCHANT:85; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:01; PURCHASEAMOUNT:150000.00; STATUSCODE:5510; PAYMENTCODE:; AMOUNT:150000.00; WORDS:d2de0cac09613f2948d43c39a8d48c3f70faef00; SESSIONID:43belnni1qvavuakg1nti6eu57; ', '2018-04-02 12:30:06'),
(12, 'DOKU_Identify', 'TRANSIDMERCHANT:93; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:15; PAYMENTCODE:; AMOUNT:50000.00; SESSIONID:ve58ou1k7j3t7r3hdn8sshk3v7; ', '2018-04-03 04:37:12'),
(13, 'DOKU_Identify', 'TRANSIDMERCHANT:96; PURCHASECURRENCY:360; CURRENCY:360; PAYMENTCHANNEL:35; PAYMENTCODE:8888870811620080; AMOUNT:3000.00; SESSIONID:6o412fna415vgt9caot3llnim6; ', '2018-04-05 14:23:13');

-- --------------------------------------------------------

--
-- Table structure for table `doku_user`
--

CREATE TABLE `doku_user` (
  `doku_user_id` int(10) NOT NULL,
  `fullname` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(10) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedby` int(11) NOT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doku_user`
--

INSERT INTO `doku_user` (`doku_user_id`, `fullname`, `username`, `password`, `created`, `createdby`, `updated`, `updatedby`, `isactive`) VALUES
(2, 'Transparacy International Indonesia', 'info@ti.or.id', 'a4d80eac9ab26a4a2da04125bc2c096a', '2018-03-20 07:10:52', 0, '2018-03-20 07:10:52', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doku_donatur`
--
ALTER TABLE `doku_donatur`
  ADD PRIMARY KEY (`doku_donatur_id`);

--
-- Indexes for table `doku_gift`
--
ALTER TABLE `doku_gift`
  ADD PRIMARY KEY (`doku_gift_id`);

--
-- Indexes for table `doku_log`
--
ALTER TABLE `doku_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doku_user`
--
ALTER TABLE `doku_user`
  ADD PRIMARY KEY (`doku_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doku_donatur`
--
ALTER TABLE `doku_donatur`
  MODIFY `doku_donatur_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `doku_gift`
--
ALTER TABLE `doku_gift`
  MODIFY `doku_gift_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `doku_log`
--
ALTER TABLE `doku_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `doku_user`
--
ALTER TABLE `doku_user`
  MODIFY `doku_user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
