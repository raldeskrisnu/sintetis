<?php
include '../config.php';
$selectdb = mysql_select_db($dbname);
if (!isset($_SESSION['doku_user_id'])) {
	echo '<script>
		alert("Sesi Anda telah berakhir.");
		window.location = "login.php";
		</script>';
} else {
	if (isset($_GET['action']) && $_GET['action']=="activated" && isset($_GET['id'])) {
		echo '<script type="text/javascript">
			var r = confirm("Apakah Anda yakin mengaktifkan dan mempublikasikan hadiah ini ?");
			if (r==true) {
				window.location = "gift_process.php?action=activated_yes&id='.$_GET['id'].'";
			} else {
				window.history.back();
			}
			</script>';
	} else if (isset($_GET['action']) && $_GET['action']=="activated_yes" && isset($_GET['id'])) {
		$sql = "UPDATE doku_gift SET isactive=1 WHERE doku_gift_id='".$_GET['id']."'";
		if(mysql_query($sql))
		{
			echo '<script>alert("Hadiah berhasil diaktifasi.");</script>';
			echo '<script>window.location = "index.php?page=gift";</script>';
		} else {
			echo '<script>alert("Hadiah gagal diaktifasi.");
				window.history.back();</script>';
		}
		
	} else if (isset($_GET['action']) && $_GET['action']=="deactivated" && isset($_GET['id'])) {
		echo '<script type="text/javascript">
			var r = confirm("Apakah Anda yakin tidak mengaktifkan dan mempublikasikan hadiah ini ?");
			if (r==true) {
				window.location = "gift_process.php?action=deactivated_yes&id='.$_GET['id'].'";
			} else {
				window.history.back();
			}
			</script>';
	} else if (isset($_GET['action']) && $_GET['action']=="deactivated_yes" && isset($_GET['id'])) {
		$sql = "UPDATE doku_gift SET isactive=0 WHERE doku_gift_id='".$_GET['id']."'";
		if(mysql_query($sql))
		{
			echo '<script>alert("Hadiah berhasil di-non-aktifakan.");</script>';
			echo '<script>window.location = "index.php?page=gift";</script>';

		} else {
			echo '<script>alert("Hadiah gagal di-non-aktifkan.");
				window.history.back();</script>';
		}
		
	} else if(isset($_GET['action']) && $_GET['action']=="deleted" && isset($_GET['id'])) {
		$selectsql = "SELECT * FROM doku_gift WHERE doku_gift_id='".$_GET['id']."'";
		if($result = @mysql_query($selectsql)){
			if(mysql_num_rows($result) > 0){
				while($row = mysql_fetch_array($result)){
					$locationFile = $row['gift_file'];
				}

				$sql = "DELETE FROM doku_gift WHERE doku_gift_id='".$_GET['id']."'";
				if(mysql_query($sql))
				{
					$targetFile = "../asset/upload/".$locationFile;
					if(file_exists($targetFile)){
						unlink($targetFile);
						echo '<script>alert("Data berhasil dihapus.");window.history.back();</script>';
					} else {
						echo '<script>alert("Data tidak ditemukan.");window.history.back();</script>';
					}

				} else {
					echo '<script>alert("Hadiah gagal di-non-aktifkan.");window.history.back();</script>';
				}
			} else {
					echo '<script>alert("Data tidak ditemukan.");window.history.back();</script>';
			}
		} else {
			echo '<script>alert("Data tidak ditemukan.");
				window.history.back();</script>';
		}

	} else if(isset($_GET['action']) && $_GET['action']=="updated" && isset($_GET['id'])){
		ob_start();
		$_SESSION['doku_gift_id']=$_GET['id'];
		echo '<script>window.location = "index.php?page=gift_update"</script>';

	} else {
		$gift_title = trim(addslashes($_POST['gift_title']));
		$gift_message = trim(addslashes($_POST['gift_message']));
		$isactive = isset($_POST['isactive']) ? $_POST['isactive'] : 0;
		$category = trim(addslashes($_POST['option']));
		if (empty($gift_title) || empty($gift_message) || empty($category)) {
			echo '<script>alert("Nama dan keterangan hadiah harus diisi.");
				window.history.back();</script>';
		} else {
			$target_dir = "../asset/upload/";
			$check_dir = scandir($target_dir);
			$file_count = count($check_dir);
			$target_file = $target_dir . basename($_FILES["gift_file"]["name"]);
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			$target_name = 'GIFT-'.date('YmdHis').$file_count.'.'.$imageFileType;
			if(isset($_POST["simpan"])) {
			    $check = getimagesize($_FILES["gift_file"]["tmp_name"]);
			    if($check === false) {
			        echo '<script>alert("Berkas bukan sebuah gambar.");
						window.history.back();</script>';
			    }
			}

			if (file_exists($target_name)) {
			    echo '<script>alert("Maaf, berkas sudah tersedia.");
					window.history.back();</script>';
			}
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
				echo '<script>alert("Maaf, hanya format JPG, JPEG, PNG & GIF yang diizinkan.");
					window.history.back();</script>';
			}
		    if (!move_uploaded_file($_FILES["gift_file"]["tmp_name"], $target_dir . $target_name)) {
		        echo '<script>alert("Maaf, ada masalah dalam pengunggahan berkas '.$_FILES["gift_file"]["name"].'.");
					window.history.back();</script>';
		    }
			$sql = "INSERT INTO doku_gift (gift_title,gift_message,gift_file,isactive,category) VALUES ('".$gift_title."','".$gift_message."','".$target_name."','".$isactive."','".$category."')";

			if(mysql_query($sql)) {
				echo '<script>alert("Hadiah baru berhasil disimpan.");</script>';
				if ($_POST['simpan']=="simpan") {
					echo '<script>window.location = "index.php?page=gift";</script>';
				} elseif ($_POST['simpan']=="tambah") {
					echo '<script>window.location = "index.php?page=gift_new";</script>';
				}
			} else {
				echo '<script>alert("Hadiah baru gagal disimpan.");
					window.history.back();</script>';
			}
			
		}
	}
}
?>