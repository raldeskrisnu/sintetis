        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Atur ulang kata sandi pengguna</h3>
            </div>
            <form action="users_changepass.php" role="form" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputTitle">Kata sandi sekarang</label>
                  <input type="password" class="form-control" id="inputTitle" placeholder="Masukkan kata sandi sekarang" name="currentpassword">
                </div>
                <div class="form-group">
                  <label for="inputTitle">Kata sandi baru</label>
                  <input type="password" class="form-control" id="inputTitle" placeholder="Masukkan kata sandi baru" name="newpassword">
                </div>
                <div class="form-group">
                  <label for="inputTitle">Konfirmasi kata sandi baru</label>
                  <input type="password" class="form-control" id="inputTitle" placeholder="Masukkan konfirmasi kata sandi baru" name="confirmpassword">
                </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary" name="simpan" value="simpan">Simpan</button>
                <a class="btn btn-danger" href="index.php?page=users">Batal</a>
              </div>
            </form>
          </div>
        </div>