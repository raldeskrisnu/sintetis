<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../asset/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../asset/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../asset/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../asset/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="../asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
</head>
<body class="hold-transition skin-blue">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Administrator</h1>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Menu</h3>
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="index.php?page=home"><i class="fa fa-dashboard"></i> Beranda</a></li>
                <li><a href="index.php?page=users"><i class="fa fa-users"></i> Pengguna</a></li>
                <li><a href="index.php?page=gift"><i class="fa fa-gear"></i> Pengaturan Hadiah</a></li>
                <li><a href="index.php?page=confirmation"><i class="fa fa-check"></i> Konfirmasi Donasi</a></li>
                <li><a href="index.php?page=report"><i class="fa fa-file"></i> Laporan</a></li>
                <li><a href="logout.php"><i class="fa fa-sign-out"></i> Keluar</a></li>
              </ul>
            </div>
          </div>
        </div>