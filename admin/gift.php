        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Hadiah</h3>
            </div>
            <div class="box-body">
              <a class="btn btn-app btn-success" href="index.php?page=gift_new">
                <i class="fa fa-asterisk"></i> Buat Baru
              </a>
              <table id="list" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Gambar</th>
                    <th>Judul</th>
                    <th>Kategori</th>
                    <th>Deskripsi</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $selectdb = mysql_select_db($dbname);
                  $sql = "SELECT * FROM doku_gift";
                  if($result = @mysql_query($sql)){
                      if(mysql_num_rows($result) > 0){
                        while($row = mysql_fetch_array($result)){
                            echo "<tr>";
                            echo "<td><img src='../asset/upload/".$row['gift_file']."' alt='".$row['gift_file']."' width='200' height='200' /></td>";
                            echo "<td>".$row['gift_title']."</td>";
                            echo "<td>".$row['category']."</td>";
                            echo "<td>".$row['gift_message']."</td>";
                                if ($row['isactive']=="1") {
                                    echo "<td><a href='gift_process.php?action=deactivated&id=".$row['doku_gift_id']."' class='btn btn-success'>Aktif</a></td>";
                                } else {
                                    echo "<td><a href='gift_process.php?action=activated&id=".$row['doku_gift_id']."' class='btn btn-danger'>Non Aktif</a></td>";
                                }
                            echo "<td>
                            <a href='gift_process.php?action=deleted&id=".$row['doku_gift_id']."' class='btn btn-danger'>Delete</a>
                            <a href='gift_process.php?action=updated&id=".$row['doku_gift_id']."' class='btn btn-primary'>Update</a>
                  
                            </td>";
                             echo "</tr>";
                        }
                      } else {
                        echo "0 results";
                      }
                  }
                
                  ?>
                </tbody>
                <tfoot>
                  
                </tfoot>
              </table>
            </div>
          </div>
        </div>