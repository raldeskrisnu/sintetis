        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Buat Baru Pengguna</h3>
            </div>
            <form action="users_process.php" role="form" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputTitle">Nama Lengkap</label>
                  <input type="text" class="form-control" id="inputTitle" placeholder="Masukkan nama lengkap" name="fullname">
                </div>
                <div class="form-group">
                  <label for="inputEmail">Email Pengguna</label>
                  <input type="email" class="form-control" id="inputEmail" placeholder="Masukkan nama pengguna" name="username">
                </div>
                <div class="form-group">
                  <label for="inputPassword">Kata Sandi</label>
                  <input type="password" class="form-control" id="inputPassword" placeholder="Masukkan kata sandi" name="password">
                </div>
                <div class="form-group">
                  <label for="inputConfirm">Kata Sandi</label>
                  <input type="password" class="form-control" id="inputConfirm" placeholder="Masukkan kata sandi kembali" name="confirm">
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="isactive" value="1"> Aktif dan publikasikan
                  </label>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary" name="simpan" value="simpan">Simpan</button>
                <button type="submit" class="btn btn-success" name="simpan" value="tambah">Simpan dan Tambah</button>
                <a class="btn btn-danger" href="index.php?page=users">Batal</a>
              </div>
            </form>
          </div>
        </div>