<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Donasi</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="asset/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="asset/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="asset/dist/css/AdminLTE.min.css">
</head>
<body class="hold-transition skin-blue">
<div class="wrapper">
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Donasi</h1>
    </section>
    <section class="content">
      <form method="POST" action="payment_transfer" role="form">
        <div class="box box-default">
          <div class="box-header with-border" align="center">
            <div class="row">
              <div class="col-sm-4">
                <a href="index" id="primary" class="btn btn-default btn-lg btn-block">Donasi Rutin</a>
              </div>
              <div class="col-sm-4">
                <a href="donasisekali" id="warning" class="btn btn-warning btn-lg btn-block">Donasi Sekali</a>
              </div>
              <div class="col-sm-4">
              <a href="donasiprogram" id="danger" class="btn btn-default btn-lg btn-block">Donasi Program</a>
              </div>
            </div>
          </div>
        </div>

        <div class="box box-default">
        <div class="row">
          <div class="col-md-6">
              <div class="box-body">
                <div class="row">
                  <div class="col-sm-3">
                    <label>
                      <input type="radio" name="donatype" value="Individu"> Individu
                    </label>
                  </div>
                  <div class="col-sm-3">
                    <label>
                      <input type="radio" name="donatype" value="Lembaga"> Lembaga
                    </label>
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Nama Individu/Lembaga" name="fullname" required="true">
                  </div>
                </div>
                <br />
                <div class="row">
                  <div class="col-sm-12">
                    <input type="email" class="form-control" placeholder="Alamat Email" name="email" required="true">
                  </div>
                </div>
                <br />
                <div class="row">
                  <div class="col-sm-4">
                    <input type="text" class="form-control" placeholder="Kode Area" name="areacode" required="true">
                  </div>
                  <div class="col-sm-8">
                  <input type="text" class="form-control" placeholder="Nomor Telephone" onkeypress='validate(event)' name="telp" required="true" >
                  </div>
                </div>
                <br />
                <div class="row">
                  <div class="col-sm-12">
                    <textarea class="form-control" placeholder="Alamat" name="address" required="true"></textarea>
                  </div>
                </div>
                <br />
                <div class="row">
                  <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Kota" name="city" required="true">
                  </div>
                  <div class="col-sm-6">
                    <select name="country" class="form-control" required="true">
                    <?php
                    $arrCountryCodes = array('AF' => 'AFGHANISTAN', 'AL' => 'ALBANIA', 'DZ' => 'ALGERIA', 'AS' => 'AMERICAN SAMOA', 'AD' => 'ANDORRA', 'AO' => 'ANGOLA', 'AI' => 'ANGUILLA', 'AQ' => 'ANTARCTICA', 'AG' => 'ANTIGUA AND BARBUDA', 'AR' => 'ARGENTINA', 'AM' => 'ARMENIA', 'AW' => 'ARUBA', 'AU' => 'AUSTRALIA', 'AT' => 'AUSTRIA', 'AZ' => 'AZERBAIJAN', 'BS' => 'BAHAMAS', 'BH' => 'BAHRAIN', 'BD' => 'BANGLADESH', 'BB' => 'BARBADOS', 'BY' => 'BELARUS', 'BE' => 'BELGIUM', 'BZ' => 'BELIZE', 'BJ' => 'BENIN', 'BM' => 'BERMUDA', 'BT' => 'BHUTAN', 'BO' => 'BOLIVIA, PLURINATIONAL STATE OF', 'BQ' => 'BONAIRE, SINT EUSTATIUS AND SABA', 'BA' => 'BOSNIA AND HERZEGOVINA', 'BW' => 'BOTSWANA', 'BV' => 'BOUVET ISLAND', 'BR' => 'BRAZIL', 'IO' => 'BRITISH INDIAN OCEAN TERRITORY', 'BN' => 'BRUNEI DARUSSALAM', 'BG' => 'BULGARIA', 'BF' => 'BURKINA FASO', 'BI' => 'BURUNDI', 'KH' => 'CAMBODIA', 'CM' => 'CAMEROON', 'CA' => 'CANADA', 'CV' => 'CAPE VERDE', 'KY' => 'CAYMAN ISLANDS', 'CF' => 'CENTRAL AFRICAN REPUBLIC', 'TD' => 'CHAD', 'CL' => 'CHILE', 'CN' => 'CHINA', 'CX' => 'CHRISTMAS ISLAND', 'CC' => 'COCOS (KEELING) ISLANDS', 'CO' => 'COLOMBIA', 'KM' => 'COMOROS', 'CG' => 'CONGO', 'CD' => 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'CK' => 'COOK ISLANDS', 'CR' => 'COSTA RICA', 'CI' => 'COTE DIVOIRE', 'HR' => 'CROATIA', 'CU' => 'CUBA', 'CW' => 'CURACAO', 'CY' => 'CYPRUS', 'CZ' => 'CZECH REPUBLIC', 'DK' => 'DENMARK', 'DJ' => 'DJIBOUTI', 'DM' => 'DOMINICA', 'DO' => 'DOMINICAN REPUBLIC', 'EC' => 'ECUADOR', 'EG' => 'EGYPT', 'SV' => 'EL SALVADOR', 'GQ' => 'EQUATORIAL GUINEA', 'ER' => 'ERITREA', 'EE' => 'ESTONIA', 'ET' => 'ETHIOPIA', 'FK' => 'FALKLAND ISLANDS (MALVINAS)', 'FO' => 'FAROE ISLANDS', 'FJ' => 'FIJI', 'FI' => 'FINLAND', 'FR' => 'FRANCE', 'GF' => 'FRENCH GUIANA', 'PF' => 'FRENCH POLYNESIA', 'TF' => 'FRENCH SOUTHERN TERRITORIES', 'GA' => 'GABON', 'GM' => 'GAMBIA', 'GE' => 'GEORGIA', 'DE' => 'GERMANY', 'GH' => 'GHANA', 'GI' => 'GIBRALTAR', 'GR' => 'GREECE', 'GL' => 'GREENLAND', 'GD' => 'GRENADA', 'GP' => 'GUADELOUPE', 'GU' => 'GUAM', 'GT' => 'GUATEMALA', 'GG' => 'GUERNSEY', 'GN' => 'GUINEA', 'GW' => 'GUINEA-BISSAU', 'GY' => 'GUYANA', 'HT' => 'HAITI', 'HM' => 'HEARD ISLAND AND MCDONALD ISLANDS', 'VA' => 'HOLY SEE (VATICAN CITY STATE)', 'HN' => 'HONDURAS', 'HK' => 'HONG KONG', 'HU' => 'HUNGARY', 'IS' => 'ICELAND', 'IN' => 'INDIA', 'ID' => 'INDONESIA', 'IR' => 'IRAN, ISLAMIC REPUBLIC OF', 'IQ' => 'IRAQ', 'IE' => 'IRELAND', 'IM' => 'ISLE OF MAN', 'IL' => 'ISRAEL', 'IT' => 'ITALY', 'JM' => 'JAMAICA', 'JP' => 'JAPAN', 'JE' => 'JERSEY', 'JO' => 'JORDAN', 'KZ' => 'KAZAKHSTAN', 'KE' => 'KENYA', 'KI' => 'KIRIBATI', 'KP' => 'KOREA, DEMOCRATIC PEOPLES REPUBLIC OF', 'KR' => 'KOREA, REPUBLIC OF', 'KW' => 'KUWAIT', 'KG' => 'KYRGYZSTAN', 'LA' => 'LAO PEOPLES DEMOCRATIC REPUBLIC', 'LV' => 'LATVIA', 'LB' => 'LEBANON', 'LS' => 'LESOTHO', 'LR' => 'LIBERIA', 'LY' => 'LIBYA', 'LI' => 'LIECHTENSTEIN', 'LT' => 'LITHUANIA', 'LU' => 'LUXEMBOURG', 'MO' => 'MACAO', 'MK' => 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'MG' => 'MADAGASCAR', 'MW' => 'MALAWI', 'MY' => 'MALAYSIA', 'MV' => 'MALDIVES', 'ML' => 'MALI', 'MT' => 'MALTA', 'MH' => 'MARSHALL ISLANDS', 'MQ' => 'MARTINIQUE', 'MR' => 'MAURITANIA', 'MU' => 'MAURITIUS', 'YT' => 'MAYOTTE', 'MX' => 'MEXICO', 'FM' => 'MICRONESIA, FEDERATED STATES OF', 'MD' => 'MOLDOVA, REPUBLIC OF', 'MC' => 'MONACO', 'MN' => 'MONGOLIA', 'ME' => 'MONTENEGRO', 'MS' => 'MONTSERRAT', 'MA' => 'MOROCCO', 'MZ' => 'MOZAMBIQUE', 'MM' => 'MYANMAR', 'NA' => 'NAMIBIA', 'NR' => 'NAURU', 'NP' => 'NEPAL', 'NL' => 'NETHERLANDS', 'NC' => 'NEW CALEDONIA', 'NZ' => 'NEW ZEALAND', 'NI' => 'NICARAGUA', 'NE' => 'NIGER', 'NG' => 'NIGERIA', 'NU' => 'NIUE', 'NF' => 'NORFOLK ISLAND', 'MP' => 'NORTHERN MARIANA ISLANDS', 'NO' => 'NORWAY', 'OM' => 'OMAN', 'PK' => 'PAKISTAN', 'PW' => 'PALAU', 'PS' => 'PALESTINE, STATE OF', 'PA' => 'PANAMA', 'PG' => 'PAPUA NEW GUINEA', 'PY' => 'PARAGUAY', 'PE' => 'PERU', 'PH' => 'PHILIPPINES', 'PN' => 'PITCAIRN', 'PL' => 'POLAND', 'PT' => 'PORTUGAL', 'PR' => 'PUERTO RICO', 'QA' => 'QATAR', 'RE' => 'REUNION', 'RO' => 'ROMANIA', 'RU' => 'RUSSIAN FEDERATION', 'RW' => 'RWANDA', 'BL' => 'SAINT BARTH√âLEMY', 'SH' => 'SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA', 'KN' => 'SAINT KITTS AND NEVIS', 'LC' => 'SAINT LUCIA', 'MF' => 'SAINT MARTIN (FRENCH PART)', 'PM' => 'SAINT PIERRE AND MIQUELON', 'VC' => 'SAINT VINCENT AND THE GRENADINES', 'WS' => 'SAMOA', 'SM' => 'SAN MARINO', 'ST' => 'SAO TOME AND PRINCIPE', 'SA' => 'SAUDI ARABIA', 'SN' => 'SENEGAL', 'RS' => 'SERBIA', 'SC' => 'SEYCHELLES', 'SL' => 'SIERRA LEONE', 'SG' => 'SINGAPORE', 'SX' => 'SINT MAARTEN (DUTCH PART)', 'SK' => 'SLOVAKIA', 'SI' => 'SLOVENIA', 'SB' => 'SOLOMON ISLANDS', 'SO' => 'SOMALIA', 'ZA' => 'SOUTH AFRICA', 'GS' => 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'SS' => 'SOUTH SUDAN', 'ES' => 'SPAIN', 'LK' => 'SRI LANKA', 'SD' => 'SUDAN', 'SR' => 'SURINAME', 'SJ' => 'SVALBARD AND JAN MAYEN', 'SZ' => 'SWAZILAND', 'SE' => 'SWEDEN', 'CH' => 'SWITZERLAND', 'SY' => 'SYRIAN ARAB REPUBLIC', 'TW' => 'TAIWAN, PROVINCE OF CHINA', 'TJ' => 'TAJIKISTAN', 'TZ' => 'TANZANIA, UNITED REPUBLIC OF', 'TH' => 'THAILAND', 'TL' => 'TIMOR-LESTE', 'TG' => 'TOGO', 'TK' => 'TOKELAU', 'TO' => 'TONGA', 'TT' => 'TRINIDAD AND TOBAGO', 'TN' => 'TUNISIA', 'TR' => 'TURKEY', 'TM' => 'TURKMENISTAN', 'TC' => 'TURKS AND CAICOS ISLANDS', 'TV' => 'TUVALU', 'UG' => 'UGANDA', 'UA' => 'UKRAINE', 'AE' => 'UNITED ARAB EMIRATES', 'GB' => 'UNITED KINGDOM', 'US' => 'UNITED STATES', 'UM' => 'UNITED STATES MINOR OUTLYING ISLANDS', 'UY' => 'URUGUAY', 'UZ' => 'UZBEKISTAN', 'VU' => 'VANUATU', 'VE' => 'VENEZUELA, BOLIVARIAN REPUBLIC OF', 'VN' => 'VIET NAM', 'VG' => 'VIRGIN ISLANDS, BRITISH', 'VI' => 'VIRGIN ISLANDS, U.S.', 'WF' => 'WALLIS AND FUTUNA', 'EH' => 'WESTERN SAHARA', 'YE' => 'YEMEN', 'ZM' => 'ZAMBIA', 'ZW' => 'ZIMBABWE');
                    foreach ($arrCountryCodes as $k1=>$v1) {
                      if ($k1=='ID') {
                        echo "<option value='".$v1."' selected>".$v1."</option>";
                      } else {
                        echo "<option value='".$v1."'>".$v1."</option>";
                      }
                    }
                    ?>
                    </select>
                  </div>
                </div>
                <br>
                <h4>Besaran Donasi</h4>

                <div class="row">
                  <div class="col-sm-4">

                    <label>
                      <input type="radio" name="price" value="50000" id="50rb" onClick="check()"> Rp 50.000,-
                    </label>
                  </div>
                  <div class="col-sm-4">
                    <label>
                      <input type="radio" name="price" value="100000" id="100rb" onClick="check()"> Rp 100.000,-
                    </label>
                  </div>
                  <div class="col-sm-4">
                    <label>
                      <input type="radio" name="price" value="150000" id="150rb" onClick="check()"> Rp 150.000,-
                    </label>
                  </div>
                </div>
                <br/>
                <div class="row">
                  <div class="col-sm-4">
                    <label>
                      <input type="radio" id="otherprice" name="price" onClick="check()" value="X"> Jumlah Lainnya
                    </label>
                  </div>
                  <div class="col-sm-8">
                    <input type="number" class="form-control" name="amount" id="amount" placeholder="Jumlah Lainnya" disabled="disabled">
                  </div>
                </div>   
                <div class="row">

                  <div class="col-sm-8">
                    <label>
                      <h5 style="font-style: italic">Nb: Penulisan tidak menggunakan tanda baca (.,-)</h5>
                    </label>
                  </div>

                </div>
              </div>
               </div>
              <!-- appreciation gift -->
                <div class="col-md-6">
           
              <div class="box-header">
                <h3 class="box-title">Appreciation Gift</h3>
              </div>
              <div class="box-body">
                <div class="form-group">
                  
                  
                     <?php 
                    $selectdb = mysql_select_db($dbname);
                        $sql = "SELECT * FROM doku_gift WHERE isactive=1 AND category='Donasi Sekali'";
                        if($result = @mysql_query($sql)){
                          if(mysql_num_rows($result) > 0){
                              while($row = mysql_fetch_array($result)){
                                $location = "asset/upload/"
                    ?>
                  
                     <div class="radio" > 
                      
                        
                      <label class="col-md-6" style="display: block;" align="center">
                              <div align="center" style="padding-top: 20px;" >
                                  <p><?php echo $row['gift_title'] ?> </p>
                                  <input type="radio" name="doku_gift" value="<?=$row['doku_gift_id']?>" id="doku_gift">
                                  <?php
                                    if (!file_exists($location.$row['gift_file'])) {

                                        $defaultImage = "http://placehold.it/200x200";
                                    } else {
                                        $defaultImage = $location.$row['gift_file'];
                          
                                    }
                                  ?>
                                  <img class="attachment-img" src="<?php echo $defaultImage; ?>" alt="Attachment Image" width="200" height="200">
                                  <!-- <p>Setiap donasi minimal Rp. 150.000,- kami memberikan appreciation gift berupa kaos pintar antikorupsi, pilihan desain dan warna dapat dilihat detail disini.</p> -->
                              </div>
                        </label>  

                          
 
                      </div>
                     
                    <?php }
                      } else {
                        echo "Tidak ada hadiah";
                      }
                  }
                
                  ?>
                 
                 
                  
                </div>
              </div>
           
          </div>

           
          </div>
          
          
      </div>

        <div class="box box-default">
          <div class="box-header with-border" align="center">
            <div class="row">
              <div class="col-sm-12">
                <input type="hidden" name="product" value="Donasi Sekali" id="product">
                <input type="submit" class="btn btn-danger btn-block" value="Donate">
              </div>
            </div>
          </div>
        </div>
      </form>
    </section>
  </div>
</div>
<script src="asset/bower_components/jquery/dist/jquery.min.js"></script>
<script src="asset/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="asset/dist/js/adminlte.min.js"></script>
<script>
function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

function check() {
  if(document.getElementById("otherprice").checked){
    document.getElementById("amount").disabled=false;
     document.getElementById("otherprice").focus();
  } else {
    document.getElementById("amount").disabled=true;
    document.getElementById("amount").value = null;
  }

}
this.check();
</script>
</body>
</html>