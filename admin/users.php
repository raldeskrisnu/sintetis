        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Pengguna</h3>
            </div>
            <div class="box-body">
              <a class="btn btn-app btn-success" href="index.php?page=users_new">
                <i class="fa fa-asterisk"></i> Buat Baru
              </a>
              <table id="list" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $selectdb = mysql_select_db($dbname);
                  $sql = "SELECT * FROM doku_user";
                  if($result = @mysql_query($sql)){
                    if(mysql_num_rows($result) > 0){
                      while($row = mysql_fetch_array($result)){
                        echo "<tr>";
                        echo "<td>".$row['fullname']."</td>";
                        echo "<td>".$row['username']."</td>";
                        if ($row['isactive']=="1") {
                          echo "<td><a href='users_process.php?action=deactivated&id=".$row['doku_user_id']."' class='btn btn-success'>Aktif</a></td>";
                        } else {
                          echo "<td><a href='users_process.php?action=activated&id=".$row['doku_user_id']."' class='btn btn-danger'>Non Aktif</a></td>";
                        }
                        echo "<td><a href='index.php?page=users_reset&id=".$row['doku_user_id']."' class='btn btn-primary'>Atur Ulang Kata Sandi</a></td>";
                      echo "</tr>";
                      }
                    } else {
                      echo "0 results";
                    }
                    
                  }
                  ?>
                </tbody>
                <tfoot>
                  
                </tfoot>
              </table>
            </div>
          </div>
        </div>